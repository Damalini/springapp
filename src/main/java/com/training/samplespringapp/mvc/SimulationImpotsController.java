package com.training.samplespringapp.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/simulation_impots")
public class SimulationImpotsController {

	@GetMapping(value={"", "/"})
	public String get() {
		return "modules/simulation_impots";
	}

	@PostMapping(value={"", "/"})
	public String submit(@ModelAttribute("statut") String statut,
						@ModelAttribute("revenu1") Integer revenu1,
						@ModelAttribute("revenu2") Integer revenu2,
						@ModelAttribute("nombreEnfants") Integer nombreEnfants,
						Model model) {
		int revenu = revenu1;
		if (statut.toLowerCase().equals("couple")) {
			revenu += revenu2;
		}
		double parts = calculNombreParts(statut.toLowerCase().equals("couple"), nombreEnfants);
		int impot = calculImpotRevenu(revenu, parts);
		model.addAttribute("statut", statut);
		model.addAttribute("revenu", revenu);
		model.addAttribute("parts", parts);
		model.addAttribute("impot", impot);
		return "modules/simulation_impots";
	}

	/*
		Le calcul du nombre de parts du foyer se fait de la manière suivante:
		Pour un couple, on démarre avec 2 parts et pour un célibataire, une part.
		Pour chacun des 2 premiers enfants du couple, on rajoute 0.5 part supplémentaire
		Pour les autres enfants (à partir du 3e), on rajoute 1 part supplémentaire
	*/
	public double calculNombreParts(boolean couple, int nombreEnfants) {
		double parts = couple? 2 : 1;
		if (nombreEnfants > 2) {
			parts += (nombreEnfants - 2);
			nombreEnfants = 2;
		}
		parts += nombreEnfants * 0.5;
		return parts;	
	}

	/*
		Le calcul de l'impot sur le revenu se fait selon le barème progressif par tranches d'imposition
		Le taux d'imposition est de:
			45% pour la part du revenu supérieur à 153000
			41% pour la part du revenu compris entre 72000 et 135000
			30% pour la part du revenu compris entre 27000 et 72000
			14% pour la part du revenu compris entre 9800 et 27000
			0% pour la part du revenu inférieur à 9800
		L'impot par parts sur le revenu est obtenu:
			- on calcule le revenu par part en divisant le revenu total du foyer par le nombre de parts
			- on multiplie chaque tranche du revenu obtenu par son taux d'imposition
			- on additionne les impots obtenus
		Pour obtenir l'impot final il suffit donc de multiplier l'impot par part par le nombre de parts.
	*/
	public int calculImpotRevenu(double revenue, double parts) {
		double impots = 0;
		revenue = revenue / parts;
		if (revenue > 153000) {
			impots += (revenue - 153000)*0.45;
			revenue = 153000;
		}
		if (revenue > 72000) {
			impots += (revenue - 72000)*0.41;
			revenue = 72000;
		}
		if (revenue > 27000) {
			impots += (revenue - 27000)*0.3;
			revenue = 27000;
		}
		if (revenue > 9800) {
			impots += (revenue - 9800)*0.14;
		}
		return (int)(impots * parts);
	}
	
}
