<!DOCTYPE html>
<html>
	<head>
		<title>Exemple Appli Spring</title>
		<link rel="stylesheet" href="../../css/bootstrap.min.css" />
	</head>
	<body>
		<div class="container" style="margin-left:10%">
			
			<nav class="navbar navbar-dark bg-primary" style="margin-bottom:30px">
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item"><a class="nav-link" href="/">Exemple Appli Spring</a></li>
				</ul>
			</nav>
			
			<h4 class="mt-2" style="margin-bottom:30px">Simulation de l'impôt sur le revenu</h4>
			
			<#if impot?? >
			
			<div style="margin-left:5%">
				<p style="margin-bottom:15px">Résultats de la simulation</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">Paramètre</th>
							<th scope="col">Valeur</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Statut</td>
							<td>${statut}</td>
						</tr>
						<tr>
							<td>Revenus du foyer</td>
							<td>${revenu} euros</td>
						</tr>
						<tr>
							<td>Nombre de parts</td>
							<td>${parts}</td>
						</tr>
						<tr>
							<td>Impôt sur le revenu</td>
							<td>${impot}</td>
						</tr>
					</tbody>
				</table>
				
				<p style="margin-top:15px">Pour retourner à la simulation <a href="">cliquez ici</a></p>
				
			</div>
			
			<#else>
			
			<div style="margin-left:5%">
			<form class="needs-validation" action="" method="post">
				<div class="custom-control custom-radio">
					<input class="custom-control-input" type="radio" name="statut" id="statutSingle" value="Célibataire" checked>
					<label class="custom-control-label" for="statutSingle">Célibataire</label>
				</div>
				<div class="custom-control custom-radio" style="margin-bottom:20px">
					<input class="custom-control-input" type="radio" name="statut" id="statutMarried" value="Couple">
					<label class="custom-control-label" for="statutMarried">Couple</label>
				</div>
				
				<div class="form-group">
					<label for="revenu1">Revenu fiscal de reference:</label><br/>
					<input id="revenu1" type="number" min="0" name="revenu1" placeholder="Montant en euros" required/>
				</div>
				
				<div class="form-group">
					<label for="revenu2">Revenu fiscal de reference du conjoint:</label><br/>
					<input id="revenu2" type="number" min="0" name="revenu2" placeholder="Montant en euros" required/>
				</div>
				
				<div class="form-group">
					<label for="nombreEnfants">Nombre d'enfants à charge:</label><br/>
					<input id="nombreEnfants" type="number" min="0" name="nombreEnfants" placeholder="Nombre d'enfants à charge" required/>
				</div>
				
				<button type="submit" class="btn btn-primary">Calculer l'impôt</button>
			</form>
			</div>
			
			</#if>
		</div>
	</body>
</html>
